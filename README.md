## **Основная информация**
|         ФИО             | Город  | Возраст |    Образование    |   Место работы    |      Мотивация                |
|-------------------------|--------|---------|-------------------|-------------------|-------------------------------|
| Бодрова Олеся Сергеевна | Минск  | 35      | Высшее/психология | Softmax/QA manual | Повысить свой проф. уровень   | 
|                         |        |         |                   |                   | Освоить новые инструменты     |
|                         |        |         |                   |                   |                               |
|-------------------------|--------|---------|-------------------|-------------------|-------------------------------|  

[Профиль www.linkedin.com](https://www.linkedin.com/in/alesiabadrova/)


## **Все README.md файлы  см. тут:**
* [java_intro](https://bitbucket.org/AlesiaB/java-automation/src/master/homeworks/java_intro/README.md)
* [testing](https://bitbucket.org/AlesiaB/java-automation/src/master/homeworks/testing/README.md)
* [unit_testing_framework](https://bitbucket.org/AlesiaB/java-automation/src/master/homeworks/unit_testing_framework/README.md)
* [db](https://bitbucket.org/AlesiaB/java-automation/src/master/homeworks/db/README.md)
* [jdbc_xml](https://bitbucket.org/AlesiaB/java-automation/src/master/homeworks/jdbc_xml/README.md)


## **Заметки**

### Git – основы
* ```bash
* git clone git@bitbucket.org:YOUR_NAME/java-automation.git
* git status
* git add file.txt
* git log
* git log --pretty=oneline
* git log --pretty=oneline --all
* git log --graph --oneline --all
* git commit -m "MESSAGE"
* git commit --amend -m "MESSAGE"
* git checkout REVESION
* git checkout BRANCH
* git checkout -b BRANCH
* git tag v1
* git tag
* git reset HEAD file.txt
* git revert HEAD --no-edit
* git push
* git push -u origin BRANCH
* git merge BRANCH
* git rebase BRANCH
* git rebase -i BRANCH
* git fetch
* git pull
* git diff
* git rm
* git stash
* git grep -n 111
* git cherry-pick e43a6
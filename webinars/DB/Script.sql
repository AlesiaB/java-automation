CREATE TABLE gender (
	gender_id TEXT(1) NOT NULL PRIMARY KEY,
	name TEXT(255) NOT NULL
);

CREATE TABLE person (
	person_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	last_name TEXT(255) NOT NULL,
	gender TEXT(1) NOT NULL,
	FOREIGN KEY(gender) REFERENCES gender(gender_id)
);

CREATE TABLE workplace (
	workplace_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	org_name TEXT(255) NOT NULL
);

CREATE TABLE person_workplace (
	person_workplace_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	workplace_id INTEGER NOT NULL REFERENCES workplace(workplace_id),
	person_id INTEGER NOT NULL REFERENCES person(person_id)
);

ALTER TABLE person ADD first_name TEXT(255);

INSERT INTO gender(gender_id, name) VALUES ("m", "man");
INSERT INTO gender(gender_id, name) VALUES ("w", "woman");
INSERT INTO gender(gender_id, name) VALUES ("u", "unknown");

INSERT INTO person (first_name, last_name , gender) VALUES ("�������", "������", "m");
INSERT INTO person (first_name, last_name , gender) VALUES ("�������", "������", "m");
INSERT INTO person (first_name, last_name , gender) VALUES ("����", "��������", "m");
INSERT INTO person (first_name, last_name , gender) VALUES ("��������", "�������", "m");
INSERT INTO person (first_name, last_name , gender) VALUES ("�����", "��������", "w");
INSERT INTO person (first_name, last_name , gender) VALUES ("���������", "���������", "u");

INSERT INTO workplace (org_name) VALUES ("EPAM");
INSERT INTO workplace (org_name) VALUES ("Exposit");
INSERT INTO workplace (org_name) VALUES ("Wargaming");

INSERT INTO person_workplace (workplace_id, person_id) VALUES (1, 1);
INSERT INTO person_workplace (workplace_id, person_id) VALUES (2, 1);
INSERT INTO person_workplace (workplace_id, person_id) VALUES (3, 2);
INSERT INTO person_workplace (workplace_id, person_id) VALUES (4, 2);
INSERT INTO person_workplace (workplace_id, person_id) VALUES (5, 3);
INSERT INTO person_workplace (workplace_id, person_id) VALUES (6, 3);

UPDATE workplace SET org_name="Epam" WHERE org_name="EPAM";

SELECT * FROM person;

SELECT first_name, last_name FROM person;

SELECT first_name, last_name FROM person WHERE gender = "w";

SELECT first_name, last_name FROM person WHERE person_id IN (3,4);

SELECT person_id, first_name, last_name FROM person WHERE person_id BETWEEN 1 AND 4;

UPDATE person SET first_name ="��������" WHERE first_name ="��������";

SELECT person_id, first_name, last_name FROM person WHERE person_id BETWEEN 1 AND 4;

SELECT person_id, first_name, last_name FROM person WHERE last_name LIKE '%��';

SELECT *
FROM person 
WHERE last_name LIKE '%��'
ORDER BY last_name DESC;

SELECT DISTINCT gender
FROM person
ORDER BY gender DESC;

SELECT first_name, gender.name AS gender_name
FROM person
JOIN gender ON person.gender = gender.gender_id;

SELECT first_name, gender.name AS gender_name, workplace.org_name AS work_org_name
FROM person
JOIN gender ON person.gender = gender.gender_id
JOIN person_workplace ON person.person_id = person_workplace.person_id 
JOIN workplace ON person_workplace.workplace_id = workplace.workplace_id;

SELECT workplace.org_name AS work_org_name, COUNT(*) 
FROM person
JOIN gender ON person.gender = gender.gender_id
JOIN person_workplace ON person.person_id = person_workplace.person_id 
JOIN workplace ON person_workplace.workplace_id = workplace.workplace_id
GROUP BY workplace .org_name;

SELECT * 
FROM person
WHERE first_name = '�������'
UNION 
SELECT * 
FROM person
WHERE first_name = '����';

SELECT * 
FROM person
WHERE first_name = '�������'
INTERSECT 
SELECT * 
FROM person
WHERE first_name = '����' OR first_name = '�������';

SELECT * 
FROM person
WHERE first_name = '����' OR first_name = '�������'
EXCEPT 
SELECT * 
FROM person
WHERE first_name = '�������';

SELECT SUM(person_id) AS sum_of_ids
FROM person;














































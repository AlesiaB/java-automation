public class ClassWithPrivateConstructor {

    private int a;

    public ClassWithPrivateConstructor(int a) {
        this.a = a;
    }

    private ClassWithPrivateConstructor() {
    }

    public static void help() {

    }

    public static void help(String a) {

    }
}

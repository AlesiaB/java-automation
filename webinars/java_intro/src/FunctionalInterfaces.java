import java.util.Scanner;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class FunctionalInterfaces {

    public static void main(String[] args) {

        Consumer<Integer> printer = x -> System.out.println("Value = " + x);
        printer.accept(65);
        printer.accept(89);

        Predicate<Integer> isPositive = x -> x >0;
        System.out.println(isPositive.test(5));
        System.out.println(isPositive.test(- 10));

        Function<Integer, String> convert = x -> x + " долларов";
        System.out.println(convert.apply(100));

        Supplier<String> nameFactory  = () -> {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Your name ");
            return scanner.nextLine();
        };

        String name1 = nameFactory.get();
        String name2 = nameFactory.get();
        System.out.println(name1);
        System.out.println(name2);

    }
}

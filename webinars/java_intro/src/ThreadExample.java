public class ThreadExample {

    public static void main(String[] args) {

        Runnable runnable = () -> {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };

        Thread myThread = new Thread(runnable,"myThread");
        Thread myThread2 = new Thread(runnable,"myThread2");

         myThread.start();
         myThread2.start();

         myThread.interrupt();
         myThread2.interrupt();
    }
}

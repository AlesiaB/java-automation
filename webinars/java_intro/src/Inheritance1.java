public class Inheritance1 {

    protected int value = 0;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        if (checkValue(value)) {
            this.value = value;
        }
    }

    protected boolean checkValue(int value) {
        return value > 0;
    }

}

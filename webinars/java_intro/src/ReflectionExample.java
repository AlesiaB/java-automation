import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectionExample {

    public static class MyClass {

        private String name = "default";

        public MyClass(String name) {
            this.name = name;
        }

        public MyClass() {

        }

        private void printData() {
            System.out.println(this.name);
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    /* public static void main2 (String[] args) throws NoSuchFieldException, IllegalAccessException {

        MyClass myClass = new MyClass();
        myClass.setName("Some name");

        //

        String name = null;
        Field field = myClass.getClass().getDeclaredField("name");
        field.setAccessible(true);
        name = (String) field.get(myClass);

        System.out.println(name);


    }

    public static void main (String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {

        MyClass myClass = new MyClass();
        myClass.setName("Some name");

        //

        String name = null;
        Method field = myClass.getClass().getDeclaredMethod("printData");
        field.setAccessible(true);
        field.invoke(myClass);

        System.out.println(field);

    }  */

    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        MyClass myClass = null;

        Class clazz = Class.forName(MyClass.class.getName());
        Class[] params = {String.class};
        myClass = (MyClass) clazz.getConstructor(params).newInstance("Some name");

        System.out.println(myClass);

    }
}

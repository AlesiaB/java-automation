public class Polymorphism {

    public static class Burnable {

        public void burn() {
            System.out.println("Burn");
        }
    }

    public static class BurnableA extends Burnable {

    }

    public static class BurnableB extends Burnable {

    }

    public static class BurnableC extends Burnable {

    }

    public static void main(String[] args) {

        Burnable a = new BurnableA();
        Burnable b = new BurnableB();
        Burnable c = new BurnableC();

        a.burn();
        b.burn();
        c.burn();

        someFunction(a);
        someFunction(b);
        someFunction(c);

    }

    public static void someFunction(Burnable burnable) {
        burnable.burn();
    }


}

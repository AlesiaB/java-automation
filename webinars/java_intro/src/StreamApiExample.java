import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class StreamApiExample {

    public static void main(String[] args) {

        String[] arrayString = {"str1", "str2", "str12", "str3", "125"};

        List<String> listString = Arrays.stream(arrayString)
                .filter(x -> x.contains("1"))
                .collect(Collectors.toList());

        System.out.println(listString);

        IntStream.of(125, 200, 350, 400, 450)
                .filter(x -> x < 400)
                .map(x -> x +5)
                .limit(2)
                .forEach(System.out::println);
    }
}

public class Inheritance2 extends Inheritance1 {

    @Override
    protected boolean checkValue(int value) {
        return value < 0;
    }
}

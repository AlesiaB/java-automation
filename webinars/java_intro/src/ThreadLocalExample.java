public class ThreadLocalExample {

    private static ThreadLocal<Long> threadLocal = new ThreadLocal<>();

    public static void main(String[] args) {

        Runnable runnable2= () -> {
            try {
                threadLocal.set(Thread.currentThread().getId());
                threadLocal.get();
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };

        Thread thread1 = new Thread(runnable2,"Thread1");
        Thread thread2 = new Thread(runnable2,"Thread2");

        thread1.start();
        thread2.start();



    }


}

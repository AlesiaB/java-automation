import dzmitry.klokau.testing.Calculator;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorSubTests {

    private static Calculator calculator = new Calculator();

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subIntA_With_IntB(String expected, String a, String b) {
        int a1 = (int) Double.parseDouble(a);
        int b1 = (int) Double.parseDouble(b);
        int expected1 = (int) Double.parseDouble(expected);
        try {
            System.out.println("Subtraction " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a1, b1));
            assertEquals(expected1, calculator.subtraction(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subDoubleA_With_DoubleB(String expected, String a, String b) {
        double a1 = (double) Double.parseDouble(a);
        double b1 = (double) Double.parseDouble(b);
        double expected1 = (double) Double.parseDouble(expected);
        assertEquals(expected1, calculator.subtraction(a1,b1), 0.01);
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subFloatA_With_FloatB(String expected, String a, String b) {
        float a1 = Float.parseFloat(a);
        float b1 = Float.parseFloat(b);
        float expected1 = Float.parseFloat(expected);
        assertEquals(expected1, calculator.subtraction(a1,b1), 0.01);
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subShortA_With_ShortB(String expected, String a, String b) {
        short a1 = Short.parseShort(a);
        short b1 = Short.parseShort(b);
        int expected1 = Integer.parseInt(expected);
        try {
            System.out.println("Subtraction " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a1, b1));
            assertEquals(expected1, calculator.subtraction(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subCharA_With_CharB(String expected, String a, String b) {
        char a1 = (char) Integer.parseInt(a);
        char b1 = (char) Integer.parseInt(b);
        int expected1 = Integer.parseInt(expected);
        try {
            System.out.println("Subtraction " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a1, b1));
            assertEquals(expected1, calculator.subtraction(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subByteA_With_ByteB(String expected, String a, String b) {
        byte a1 = Byte.parseByte(a);
        byte b1 = Byte.parseByte(b);
        int expected1 = Integer.parseInt(expected);
        try {
            System.out.println("Subtraction " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a1, b1));
            assertEquals(expected1, calculator.subtraction(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subLongA_With_LongB(String expected, String a, String b) {
        long a1 = Long.parseLong(a);
        long b1 = Long.parseLong(b);
        long expected1 = Long.parseLong(expected);
        try {
            System.out.println("Subtraction " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a1, b1));
            assertEquals(expected1, calculator.subtraction(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subIntA_With_DoubleB(String expected, String a, String b) {
        int a1 = Integer.parseInt(a);
        double b1 = Double.parseDouble(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Subtraction " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a1, b1));
            assertEquals(expected1, calculator.subtraction(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subIntA_With_FloatB(String expected, String a, String b) {
        int a1 = Integer.parseInt(a);
        float b1 = Float.parseFloat(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Subtraction " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a1, b1));
            assertEquals(expected1, calculator.subtraction(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subIntA_With_StringB(String expected, String a, String b) {
        int a1 = Integer.parseInt(a);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Subtraction " + a1 + " and " + b + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a1, b));
            assertEquals(expected1, calculator.subtraction(a1, b), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subIntA_With_ShortB(String expected, String a, String b) {
        int a1 = Integer.parseInt(a);
        short b1 = Short.parseShort(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Subtraction " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a1, b1));
            assertEquals(expected1, calculator.subtraction(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subIntA_With_CharB(String expected, String a, String b) {
        int a1 = Integer.parseInt(a);
        char b1 = (char) Integer.parseInt(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Subtraction " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a1, b1));
            assertEquals(expected1, calculator.subtraction(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subIntA_With_ByteB(String expected, String a, String b) {
        int a1 = Integer.parseInt(a);
        byte b1 = Byte.parseByte(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Subtraction " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a1, b1));
            assertEquals(expected1, calculator.subtraction(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subIntA_With_LongB(String expected, String a, String b) {
        int a1 = Integer.parseInt(a);
        long b1 = Long.parseLong(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Subtraction " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a1, b1));
            assertEquals(expected1, calculator.subtraction(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subDoubleA_With_IntB(String expected, String a, String b) {
        double a1 = Double.parseDouble(a);
        int b1 = Integer.parseInt(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Subtraction " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a1, b1));
            assertEquals(expected1, calculator.subtraction(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subFloatA_With_IntB(String expected, String a, String b) {
        float a1 = Float.parseFloat(a);
        int b1 = Integer.parseInt(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Subtraction " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a1, b1));
            assertEquals(expected1, calculator.subtraction(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subStringA_With_IntB(String expected, String a, String b) {
        int b1 = Integer.parseInt(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Subtraction " + a + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a, b1));
            assertEquals(expected1, calculator.subtraction(a, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subShortA_With_IntB(String expected, String a, String b) {
        short a1 = Short.parseShort(a);
        int b1 = Integer.parseInt(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Subtraction " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a1, b1));
            assertEquals(expected1, calculator.subtraction(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subCharA_With_IntB(String expected, String a, String b) {
        char a1 = (char) Integer.parseInt(a);
        int b1 = Integer.parseInt(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Subtraction " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a1, b1));
            assertEquals(expected1, calculator.subtraction(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subByteA_With_IntB(String expected, String a, String b) {
        byte a1 = Byte.parseByte(a);
        int b1 = Integer.parseInt(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Subtraction " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a1, b1));
            assertEquals(expected1, calculator.subtraction(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subLongA_With_IntB(String expected, String a, String b) {
        long a1 = Long.parseLong(a);
        int b1 = Integer.parseInt(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Subtraction " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a1, b1));
            assertEquals(expected1, calculator.subtraction(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subDoubleA_With_FloatB(String expected, String a, String b) {
        double a1 = Double.parseDouble(a);
        float b1 = Float.parseFloat(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Subtraction " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a1, b1));
            assertEquals(expected1, calculator.subtraction(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subDoubleA_With_StringB(String expected, String a, String b) {
        double a1 = Double.parseDouble(a);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Subtraction " + a1 + " and " + b + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a1, b));
            assertEquals(expected1, calculator.subtraction(a1, b), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subDoubleA_With_ShortB(String expected, String a, String b) {
        double a1 = Double.parseDouble(a);
        short b1 = Short.parseShort(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Subtraction " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a1, b1));
            assertEquals(expected1, calculator.subtraction(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subDoubleA_With_CharB(String expected, String a, String b) {
        double a1 = Double.parseDouble(a);
        char b1 = (char) Integer.parseInt(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Subtraction " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a1, b1));
            assertEquals(expected1, calculator.subtraction(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subDoubleA_With_ByteB(String expected, String a, String b) {
        double a1 = Double.parseDouble(a);
        byte b1 = Byte.parseByte(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Subtraction " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a1, b1));
            assertEquals(expected1, calculator.subtraction(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subDoubleA_With_LongB(String expected, String a, String b) {
        double a1 = Double.parseDouble(a);
        long b1 = Long.parseLong(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Subtraction " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a1, b1));
            assertEquals(expected1, calculator.subtraction(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subLongA_With_DoubleB(String expected, String a, String b) {
        long a1 = Long.parseLong(a);
        double b1 = Double.parseDouble(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Subtraction " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a1, b1));
            assertEquals(expected1, calculator.subtraction(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subFloatA_With_DoubleB(String expected, String a, String b) {
        float a1 = Float.parseFloat(a);
        double b1 = Double.parseDouble(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Subtraction " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a1, b1));
            assertEquals(expected1, calculator.subtraction(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subStringA_With_DoubleB(String expected, String a, String b) {
        double b1 = Double.parseDouble(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Subtraction " + a + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a, b1));
            assertEquals(expected1, calculator.subtraction(a, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subShortA_With_DoubleB(String expected, String a, String b) {
        short a1 = Short.parseShort(a);
        double b1 = Double.parseDouble(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Subtraction " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a1, b1));
            assertEquals(expected1, calculator.subtraction(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subCharA_With_DoubleB(String expected, String a, String b) {
        char a1 = (char) Integer.parseInt(a);
        double b1 = Double.parseDouble(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Subtraction " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a1, b1));
            assertEquals(expected1, calculator.subtraction(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subByteA_With_DoubleB(String expected, String a, String b) {
        byte a1 = Byte.parseByte(a);
        double b1 = Double.parseDouble(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Subtraction " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a1, b1));
            assertEquals(expected1, calculator.subtraction(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subFloatA_With_StringB(String expected, String a, String b) {
        float a1 = Float.parseFloat(a);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Subtraction " + a1 + " and " + b + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a1, b));
            assertEquals(expected1, calculator.subtraction(a1, b), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subFloatA_With_ShortB(String expected, String a, String b) {
        float a1 = Float.parseFloat(a);
        short b1 = Short.parseShort(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Subtraction " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a1, b1));
            assertEquals(expected1, calculator.subtraction(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subFloatA_With_CharB(String expected, String a, String b) {
        float a1 = Float.parseFloat(a);
        char b1 = (char) Integer.parseInt(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Subtraction " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a1, b1));
            assertEquals(expected1, calculator.subtraction(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subFloatA_With_ByteB(String expected, String a, String b) {
        float a1 = Float.parseFloat(a);
        byte b1 = Byte.parseByte(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Subtraction " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a1, b1));
            assertEquals(expected1, calculator.subtraction(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subFloatA_With_LongB(String expected, String a, String b) {
        float a1 = Float.parseFloat(a);
        long b1 = Long.parseLong(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Subtraction " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a1, b1));
            assertEquals(expected1, calculator.subtraction(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subStringA_With_ByteB(String expected, String a, String b) {
        byte b1 = Byte.parseByte(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Subtraction " + a + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a, b1));
            assertEquals(expected1, calculator.subtraction(a, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/subtraction.csv", numLinesToSkip = 1)
    public void subStringA_With_LongB(String expected, String a, String b) {
        byte b1 = Byte.parseByte(b);
        long expected1 = Long.parseLong(expected);
        try {
            System.out.println("Subtraction " + a + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.subtraction(a, b1));
            assertEquals(expected1, calculator.subtraction(a, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }



}

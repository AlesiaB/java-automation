import dzmitry.klokau.testing.Calculator;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorMultTests {

    private static Calculator calculator = new Calculator();

    @ParameterizedTest
    @CsvFileSource(resources = "/multiplication.csv", numLinesToSkip = 1)
    public void multIntA_With_IntB(String expected, String a, String b) {
        int a1 = Integer.parseInt(a);
        int b1 = Integer.parseInt(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Multiplication " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.multiplication(a1, b1));
            assertEquals(expected1, calculator.multiplication(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/multiplication.csv", numLinesToSkip = 1)
    public void multShortA_With_IntB(String expected, String a, String b) {
        short a1 = Short.parseShort(a);
        int b1 = Integer.parseInt(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Multiplication " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.multiplication(a1, b1));
            assertEquals(expected1, calculator.multiplication(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/multiplication.csv", numLinesToSkip = 1)
    public void multCharA_With_IntB(String expected, String a, String b) {
        char a1 = (char) Integer.parseInt(a);
        int b1 = Integer.parseInt(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Multiplication " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.multiplication(a1, b1));
            assertEquals(expected1, calculator.multiplication(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/multiplication.csv", numLinesToSkip = 1)
    public void multByteA_With_IntB(String expected, String a, String b) {
        byte a1 = Byte.parseByte(a);
        int b1 = Integer.parseInt(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Multiplication " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.multiplication(a1, b1));
            assertEquals(expected1, calculator.multiplication(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/multiplication.csv", numLinesToSkip = 1)
    public void multLongA_With_IntB(String expected, String a, String b) {
        long a1 = Long.parseLong(a);
        int b1 = Integer.parseInt(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Multiplication " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.multiplication(a1, b1));
            assertEquals(expected1, calculator.multiplication(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/multiplication.csv", numLinesToSkip = 1)
    public void multDoubleA_With_FloatB(String expected, String a, String b) {
        double a1 = Double.parseDouble(a);
        float b1 = Float.parseFloat(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Multiplication " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.multiplication(a1, b1));
            assertEquals(expected1, calculator.multiplication(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/multiplication.csv", numLinesToSkip = 1)
    public void multDoubleA_With_StringB(String expected, String a, String b) {
        double a1 = Double.parseDouble(a);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Multiplication " + a1 + " and " + b + " = " + expected1);
            System.out.println("Result = " + calculator.multiplication(a1, b));
            assertEquals(expected1, calculator.multiplication(a1, b), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/multiplication.csv", numLinesToSkip = 1)
    public void multDoubleA_With_ShortB(String expected, String a, String b) {
        double a1 = Double.parseDouble(a);
        short b1 = Short.parseShort(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Multiplication " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.multiplication(a1, b1));
            assertEquals(expected1, calculator.multiplication(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/multiplication.csv", numLinesToSkip = 1)
    public void multDoubleA_With_CharB(String expected, String a, String b) {
        double a1 = Double.parseDouble(a);
        char b1 = (char) Integer.parseInt(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Multiplication " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.multiplication(a1, b1));
            assertEquals(expected1, calculator.multiplication(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/multiplication.csv", numLinesToSkip = 1)
    public void multDoubleA_With_ByteB(String expected, String a, String b) {
        double a1 = Double.parseDouble(a);
        byte b1 = Byte.parseByte(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Multiplication " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.multiplication(a1, b1));
            assertEquals(expected1, calculator.multiplication(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/multiplication.csv", numLinesToSkip = 1)
    public void multDoubleA_With_LongB(String expected, String a, String b) {
        double a1 = Double.parseDouble(a);
        long b1 = Long.parseLong(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Multiplication " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.multiplication(a1, b1));
            assertEquals(expected1, calculator.multiplication(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/multiplication.csv", numLinesToSkip = 1)
    public void multLongA_With_DoubleB(String expected, String a, String b) {
        long a1 = Long.parseLong(a);
        double b1 = Double.parseDouble(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Multiplication " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.multiplication(a1, b1));
            assertEquals(expected1, calculator.multiplication(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/multiplication.csv", numLinesToSkip = 1)
    public void multFloatA_With_DoubleB(String expected, String a, String b) {
        float a1 = Float.parseFloat(a);
        double b1 = Double.parseDouble(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Multiplication " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.multiplication(a1, b1));
            assertEquals(expected1, calculator.multiplication(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/multiplication.csv", numLinesToSkip = 1)
    public void multStringA_With_DoubleB(String expected, String a, String b) {
        double b1 = Double.parseDouble(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Multiplication " + a + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.multiplication(a, b1));
            assertEquals(expected1, calculator.multiplication(a, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/multiplication.csv", numLinesToSkip = 1)
    public void multShortA_With_DoubleB(String expected, String a, String b) {
        short a1 = Short.parseShort(a);
        double b1 = Double.parseDouble(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Multiplication " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.multiplication(a1, b1));
            assertEquals(expected1, calculator.multiplication(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/multiplication.csv", numLinesToSkip = 1)
    public void multCharA_With_DoubleB(String expected, String a, String b) {
        char a1 = (char) Integer.parseInt(a);
        double b1 = Double.parseDouble(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Multiplication " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.multiplication(a1, b1));
            assertEquals(expected1, calculator.multiplication(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/multiplication.csv", numLinesToSkip = 1)
    public void multFloatA_With_StringB(String expected, String a, String b) {
        float a1 = Float.parseFloat(a);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Multiplication " + a1 + " and " + b + " = " + expected1);
            System.out.println("Result = " + calculator.multiplication(a1, b));
            assertEquals(expected1, calculator.multiplication(a1, b), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/multiplication.csv", numLinesToSkip = 1)
    public void multLongA_With_ShortB(String expected, String a, String b) {
        float a1 = Float.parseFloat(a);
        short b1 = Short.parseShort(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Multiplication " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.multiplication(a1, b1));
            assertEquals(expected1, calculator.multiplication(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/multiplication.csv", numLinesToSkip = 1)
    public void multFloatA_With_CharB(String expected, String a, String b) {
        float a1 = Float.parseFloat(a);
        char b1 = (char) Integer.parseInt(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Multiplication " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.multiplication(a1, b1));
            assertEquals(expected1, calculator.multiplication(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/multiplication.csv", numLinesToSkip = 1)
    public void multFloatA_With_ByteB(String expected, String a, String b) {
        float a1 = Float.parseFloat(a);
        byte b1 = Byte.parseByte(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Multiplication " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.multiplication(a1, b1));
            assertEquals(expected1, calculator.multiplication(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/multiplication.csv", numLinesToSkip = 1)
    public void multFloatA_With_LongB(String expected, String a, String b) {
        float a1 = Float.parseFloat(a);
        long b1 = Long.parseLong(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Multiplication " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.multiplication(a1, b1));
            assertEquals(expected1, calculator.multiplication(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/multiplication.csv", numLinesToSkip = 1)
    public void multShortA_With_FloatB(String expected, String a, String b) {
        short a1 = Short.parseShort(a);
        float b1 = Float.parseFloat(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Multiplication " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.multiplication(a1, b1));
            assertEquals(expected1, calculator.multiplication(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/multiplication.csv", numLinesToSkip = 1)
    public void multCharA_With_FloatB(String expected, String a, String b) {
        char a1 = (char) Integer.parseInt(a);
        float b1 = Float.parseFloat(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Multiplication " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.multiplication(a1, b1));
            assertEquals(expected1, calculator.multiplication(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/multiplication.csv", numLinesToSkip = 1)
    public void multByteA_With_FloatB(String expected, String a, String b) {
        byte a1 = Byte.parseByte(a);
        float b1 = Float.parseFloat(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Multiplication " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.multiplication(a1, b1));
            assertEquals(expected1, calculator.multiplication(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/multiplication.csv", numLinesToSkip = 1)
    public void multStringA_With_ShortB(String expected, String a, String b) {
        short b1 = Short.parseShort(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Multiplication " + a + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.multiplication(a, b1));
            assertEquals(expected1, calculator.multiplication(a, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/multiplication.csv", numLinesToSkip = 1)
    public void multStringA_With_CharB(String expected, String a, String b) {
        char b1 = (char) Integer.parseInt(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Multiplication " + a + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.multiplication(a, b1));
            assertEquals(expected1, calculator.multiplication(a, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/multiplication.csv", numLinesToSkip = 1)
    public void multStringA_With_ByteB(String expected, String a, String b) {
        byte b1 = Byte.parseByte(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Multiplication " + a + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.multiplication(a, b1));
            assertEquals(expected1, calculator.multiplication(a, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/multiplication.csv", numLinesToSkip = 1)
    public void multLongA_With_StringB(String expected, String a, String b) {
        long a1 = Long.parseLong(a);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Multiplication " + a1 + " and " + b + " = " + expected1);
            System.out.println("Result = " + calculator.multiplication(a1, b));
            assertEquals(expected1, calculator.multiplication(a1, b), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/multiplication.csv", numLinesToSkip = 1)
    public void multShortA_With_StringB(String expected, String a, String b) {
        short a1 = Short.parseShort(a);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Multiplication " + a1 + " and " + b + " = " + expected1);
            System.out.println("Result = " + calculator.multiplication(a1, b));
            assertEquals(expected1, calculator.multiplication(a1, b), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

        @ParameterizedTest
        @CsvFileSource(resources = "/multiplication.csv", numLinesToSkip = 1)
        public void multCharA_With_StringB(String expected, String a, String b) {
            char a1 = (char) Integer.parseInt(a);
            double expected1 = Double.parseDouble(expected);
            try {
                System.out.println("Multiplication " + a1 + " and " + b + " = " + expected1);
                System.out.println("Result = " + calculator.multiplication(a1, b));
                assertEquals(expected1, calculator.multiplication(a1, b), 0.01);
            } catch (NumberFormatException e) {
                System.out.println(e.getMessage());
            }
        }

    @ParameterizedTest
    @CsvFileSource(resources = "/multiplication.csv", numLinesToSkip = 1)
    public void multByteA_With_StringB(String expected, String a, String b) {
        byte a1 = Byte.parseByte(a);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Multiplication " + a1 + " and " + b + " = " + expected1);
            System.out.println("Result = " + calculator.multiplication(a1, b));
            assertEquals(expected1, calculator.multiplication(a1, b), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/multiplication.csv", numLinesToSkip = 1)
    public void multShortA_With_CharB(String expected, String a, String b) {
        short a1 = Short.parseShort(a);
        char b1 = (char) Integer.parseInt(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Multiplication " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.multiplication(a1, b1));
            assertEquals(expected1, calculator.multiplication(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/multiplication.csv", numLinesToSkip = 1)
    public void multShortA_With_ByteB(String expected, String a, String b) {
        short a1 = Short.parseShort(a);
        byte b1 = Byte.parseByte(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Multiplication " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.multiplication(a1, b1));
            assertEquals(expected1, calculator.multiplication(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/multiplication.csv", numLinesToSkip = 1)
    public void multShortA_With_LongB(String expected, String a, String b) {
        short a1 = Short.parseShort(a);
        long b1 = Long.parseLong(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Multiplication " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.multiplication(a1, b1));
            assertEquals(expected1, calculator.multiplication(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }


}

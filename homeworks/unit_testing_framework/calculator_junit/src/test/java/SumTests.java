import dzmitry.klokau.testing.Calculator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(Parameterized.class)
public class SumTests {

    private Calculator calc = new Calculator();
    int a, b, expected, result;

    public SumTests(int a, int b, int expected) {
        this.a = a;
        this.b = b;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static List<Object[]> numbers() {
        return Arrays.asList(new Object[][]{
                {5, 5, 10},
                {0, 0, 0},
                {-2, 3, 1},
                {Integer.MAX_VALUE, Integer.MIN_VALUE, -1}
        });
    }

    @Before
    public void beforeTest() {
        result = 0;
    }

    @After
    public void afterTest() {
        result = 0;
    }

    @Test
    public void sumTest() {
        try {
            result = calc.sum(a, b);
            assertEquals(expected, calc.sum(a, b));
            System.out.println("sum " + a + " + " + b + " expected " + expected + " result " + result);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }


    }

}

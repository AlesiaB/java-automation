import dzmitry.klokau.testing.Calculator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorDivisionTests {

    private static Calculator calculator = new Calculator();


    @ParameterizedTest
    @CsvFileSource(resources = "/division.csv", numLinesToSkip = 1)
    public void divisionShortA_With_IntB(String expected, String a, String b) {
        short a1 = Short.parseShort(a);
        int b1 = Integer.parseInt(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Division " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.division(a1, b1));
            assertEquals(expected1, calculator.division(a1, b1));
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/division.csv", numLinesToSkip = 1)
    public void divisionChartA_With_IntB(String expected, String a, String b) {
        char a1 = (char) Integer.parseInt(a);
        int b1 = Integer.parseInt(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Division " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.division(a1, b1));
            assertEquals(expected1, calculator.division(a1, b1));
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/division.csv", numLinesToSkip = 1)
    public void divisionByteA_With_IntB(String expected, String a, String b) {
        byte a1 = Byte.parseByte(a);
        int b1 = Integer.parseInt(b);
        double expected1 = Double.parseDouble(expected);
        try {
            System.out.println("Division " + a1 + " and " + b1 + " = " + expected1);
            System.out.println("Result = " + calculator.division(a1, b1));
            assertEquals(expected1, calculator.division(a1, b1));
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/division.csv", numLinesToSkip = 1)
    public void divisionLongA_With_IntB(String expected, String a, String b) {
        long a1 = Long.parseLong(a);
        int b1 = Integer.parseInt(b);
        double expected1 = Double.parseDouble(expected);
        if (b1 != 0) {
            try {
                System.out.println("Division " + a1 + " and " + b1 + " = " + expected1);
                System.out.println("Result = " + calculator.division(a1, b1));
                assertEquals(expected1, calculator.division(a1, b1));
            } catch (NumberFormatException e) {
                System.out.println(e.getMessage());
            }
        } else {
            System.out.println("На ноль делить нельзя");
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/division.csv", numLinesToSkip = 1)
    public void divisionDoubleA_With_FloatB(String expected, String a, String b) {
        double a1 = Double.parseDouble(a);
        float b1 = Float.parseFloat(b);
        double expected1 = Double.parseDouble(expected);
        if (b1 != 0) {
            try {
                System.out.println("Division " + a1 + " and " + b1 + " = " + expected1);
                System.out.println("Result = " + calculator.division(a1, b1));
                assertEquals(expected1, calculator.division(a1, b1));
            } catch (NumberFormatException e) {
                System.out.println(e.getMessage());
            }
        } else {
            System.out.println("На ноль делить нельзя");
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/division.csv", numLinesToSkip = 1)
    public void divisionDoubleA_With_ShortB(String expected, String a, String b) {
        double a1 = Double.parseDouble(a);
        short b1 = Short.parseShort(b);
        double expected1 = Double.parseDouble(expected);
        if (b1 != 0) {
            try {
                System.out.println("Division " + a1 + " and " + b + " = " + expected1);
                System.out.println("Result = " + calculator.division(a1, b));
                assertEquals(expected1, calculator.division(a1, b));
            } catch (NumberFormatException e) {
                System.out.println(e.getMessage());
            }
        } else {
            System.out.println("На ноль делить нельзя");
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/division.csv", numLinesToSkip = 1)
    public void divisionDoubleA_With_ByteB(String expected, String a, String b) {
        double a1 = Double.parseDouble(a);
        byte b1 = Byte.parseByte(b);
        double expected1 = Double.parseDouble(expected);
        if (b1 != 0) {
            try {
                System.out.println("Division " + a1 + " and " + b1 + " = " + expected1);
                System.out.println("Result = " + calculator.division(a1, b1));
                assertEquals(expected1, calculator.division(a1, b1));
            } catch (NumberFormatException e) {
                System.out.println(e.getMessage());
            }
        } else {
            System.out.println("На ноль делить нельзя");
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/division.csv", numLinesToSkip = 1)
    public void divisionDoubleA_With_CharB(String expected, String a, String b) {
        double a1 = Double.parseDouble(a);
        char b1 = (char) Integer.parseInt(b);
        double expected1 = Double.parseDouble(expected);
        if (b1 != 0) {
            try {
                System.out.println("Division " + a1 + " and " + b1 + " = " + expected1);
                System.out.println("Result = " + calculator.division(a1, b1));
                assertEquals(expected1, calculator.division(a1, b1));
            } catch (NumberFormatException e) {
                System.out.println(e.getMessage());
            }
        } else {
            System.out.println("На ноль делить нельзя");
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/division.csv", numLinesToSkip = 1)
    public void divisionDoubleA_With_LongB(String expected, String a, String b) {
        double a1 = Double.parseDouble(a);
        long b1 = Long.parseLong(b);
        double expected1 = Double.parseDouble(expected);
        if (b1 != 0) {
            try {
                System.out.println("Division " + a1 + " and " + b1 + " = " + expected1);
                System.out.println("Result = " + calculator.division(a1, b1));
                assertEquals(expected1, calculator.division(a1, b1));
            } catch (NumberFormatException e) {
                System.out.println(e.getMessage());
            }
        } else {
            System.out.println("На ноль делить нельзя");
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/division.csv", numLinesToSkip = 1)
    public void divisionFloatA_With_DoubleB(String expected, String a, String b) {
        float a1 = Float.parseFloat(a);
        double b1 = Double.parseDouble(b);
        double expected1 = Double.parseDouble(expected);
        if (b1 != 0) {
            try {
                System.out.println("Division " + a1 + " and " + b1 + " = " + expected1);
                System.out.println("Result = " + calculator.division(a1, b1));
                assertEquals(expected1, calculator.division(a1, b1));
            } catch (NumberFormatException e) {
                System.out.println(e.getMessage());
            }
        } else {
            System.out.println("На ноль делить нельзя");
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/division.csv", numLinesToSkip = 1)
    public void divisionStringA_With_DoubleB(String expected, String a, String b) {
        double b1 = Double.parseDouble(b);
        double expected1 = Double.parseDouble(expected);
        if (b1 != 0) {
            try {
                System.out.println("Division " + a + " and " + b1 + " = " + expected1);
                System.out.println("Result = " + calculator.division(a, b1));
                assertEquals(expected1, calculator.division(a, b1));
            } catch (NumberFormatException e) {
                System.out.println(e.getMessage());
            }
        } else {
            System.out.println("На ноль делить нельзя");
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/division.csv", numLinesToSkip = 1)
    public void divisionShortA_With_DoubleB(String expected, String a, String b) {
        short a1 = Short.parseShort(a);
        double b1 = Double.parseDouble(b);
        double expected1 = Double.parseDouble(expected);
        if (b1 != 0) {
            try {
                System.out.println("Division " + a1 + " and " + b1 + " = " + expected1);
                System.out.println("Result = " + calculator.division(a1, b1));
                assertEquals(expected1, calculator.division(a1, b1));
            } catch (NumberFormatException e) {
                System.out.println(e.getMessage());
            }
        } else {
            System.out.println("На ноль делить нельзя");
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/division.csv", numLinesToSkip = 1)
    public void divisionByteA_With_DoubleB(String expected, String a, String b) {
        byte a1 = Byte.parseByte(a);
        double b1 = Double.parseDouble(b);
        double expected1 = Double.parseDouble(expected);
        if (b1 != 0) {
            try {
                System.out.println("Division " + a1 + " and " + b1 + " = " + expected1);
                System.out.println("Result = " + calculator.division(a1, b1));
                assertEquals(expected1, calculator.division(a1, b1));
            } catch (NumberFormatException e) {
                System.out.println(e.getMessage());
            }
        } else {
            System.out.println("На ноль делить нельзя");
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/division.csv", numLinesToSkip = 1)
    public void divisionLongA_With_FloatB(String expected, String a, String b) {
        long a1 = Long.parseLong(a);
        float b1 = Float.parseFloat(b);
        double expected1 = Double.parseDouble(expected);
        if (b1 != 0) {
            try {
                System.out.println("Division " + a1 + " and " + b1 + " = " + expected1);
                System.out.println("Result = " + calculator.division(a1, b1));
                assertEquals(expected1, calculator.division(a1, b1));
            } catch (NumberFormatException e) {
                System.out.println(e.getMessage());
            }
        } else {
            System.out.println("На ноль делить нельзя");
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/division.csv", numLinesToSkip = 1)
    public void divisionCharA_With_FloatB(String expected, String a, String b) {
        char a1 = (char) Integer.parseInt(a);
        float b1 = Float.parseFloat(b);
        double expected1 = Double.parseDouble(expected);
        if (b1 != 0) {
            try {
                System.out.println("Division " + a1 + " and " + b1 + " = " + expected1);
                System.out.println("Result = " + calculator.division(a1, b1));
                assertEquals(expected1, calculator.division(a1, b1));
            } catch (NumberFormatException e) {
                System.out.println(e.getMessage());
            }
        } else {
            System.out.println("На ноль делить нельзя");
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/division.csv", numLinesToSkip = 1)
    public void divisionByteA_With_FloatB(String expected, String a, String b) {
        byte a1 = Byte.parseByte(a);
        float b1 = Float.parseFloat(b);
        double expected1 = Double.parseDouble(expected);
        if (b1 != 0) {
            try {
                System.out.println("Division " + a1 + " and " + b1 + " = " + expected1);
                System.out.println("Result = " + calculator.division(a1, b1));
                assertEquals(expected1, calculator.division(a1, b1));
            } catch (NumberFormatException e) {
                System.out.println(e.getMessage());
            }
        } else {
            System.out.println("На ноль делить нельзя");
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/division.csv", numLinesToSkip = 1)
    public void divisionStringA_With_ShortB(String expected, String a, String b) {
        short b1 = Short.parseShort(b);
        double expected1 = Double.parseDouble(expected);
        if (b1 != 0) {
            try {
                System.out.println("Division " + a + " and " + b1 + " = " + expected1);
                System.out.println("Result = " + calculator.division(a, b1));
                assertEquals(expected1, calculator.division(a, b1));
            } catch (NumberFormatException e) {
                System.out.println(e.getMessage());
            }
        } else {
            System.out.println("На ноль делить нельзя");
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/division.csv", numLinesToSkip = 1)
    public void divisionStringA_With_ChartB(String expected, String a, String b) {
        char b1 = (char) Integer.parseInt(b);
        double expected1 = Double.parseDouble(expected);
        if (b1 != 0) {
            try {
                System.out.println("Division " + a + " and " + b1 + " = " + expected1);
                System.out.println("Result = " + calculator.division(a, b1));
                assertEquals(expected1, calculator.division(a, b1));
            } catch (NumberFormatException e) {
                System.out.println(e.getMessage());
            }
        } else {
            System.out.println("На ноль делить нельзя");
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/division.csv", numLinesToSkip = 1)
    public void divisionStringA_With_ByteB(String expected, String a, String b) {
        byte b1 = Byte.parseByte(b);
        double expected1 = Double.parseDouble(expected);
        if (b1 != 0) {
            try {
                System.out.println("Division " + a + " and " + b1 + " = " + expected1);
                System.out.println("Result = " + calculator.division(a, b1));
                assertEquals(expected1, calculator.division(a, b1));
            } catch (NumberFormatException e) {
                System.out.println(e.getMessage());
            }
        } else {
            System.out.println("На ноль делить нельзя");
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/division.csv", numLinesToSkip = 1)
    public void divisionStringA_With_LongB(String expected, String a, String b) {
        long b1 = Long.parseLong(b);
        double expected1 = Double.parseDouble(expected);
        if (b1 != 0) {
            try {
                System.out.println("Division " + a + " and " + b1 + " = " + expected1);
                System.out.println("Result = " + calculator.division(a, b1));
                assertEquals(expected1, calculator.division(a, b1));
            } catch (NumberFormatException e) {
                System.out.println(e.getMessage());
            }
        } else {
            System.out.println("На ноль делить нельзя");
        }
    }


}


import dzmitry.klokau.testing.Calculator;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorSumTests {

    private static Calculator calculator = new Calculator();

    @ParameterizedTest
    @CsvFileSource(resources = "/sum.csv", numLinesToSkip = 1)
    public void sumIntA_With_IntB(String expected, String a, String b) {
        int a1 = (int) Double.parseDouble(a);
        int b1 = (int) Double.parseDouble(b);
        int expected1 = (int) Double.parseDouble(expected);
        try {
            System.out.println("Sum " + a1 + " and " + b1 + " = " + expected);
            System.out.println("Result = " + calculator.sum(a1, b1));
            assertEquals(expected1, calculator.sum(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/sum.csv", numLinesToSkip = 1)
    public void sumDoubleA_With_FloatB(String expected, String a, String b) {
        double a1 = Double.parseDouble(a);
        float b1 = Float.parseFloat(b);
        double expected1 = Double.parseDouble(expected);
        System.out.println("Sum " + a1 + " and " + b1 + " = " + expected);
        try{
            System.out.println("Result = " + calculator.sum(a1, b1));
            assertEquals(expected1, calculator.sum(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/sum.csv", numLinesToSkip = 1)
    public void sumTest2(String expected, String a, String b) {
        int a1 = Integer.parseInt(a);
        long b1 = Long.parseLong(b);
        double expected1 = Double.parseDouble(expected);
        System.out.println("Sum " + a1 + " and " + b1 + " = " + expected1);
        try {
            System.out.println("Result = " + calculator.sum(a1, b1));
            assertEquals(expected1, calculator.sum(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/sum.csv", numLinesToSkip = 1)
    public void sumShortA_withShortB(String expected, String a, String b) {
        short a1 = Short.parseShort(a);
        short b1 = Short.parseShort(b);
        int expected1 = Integer.parseInt(expected);
        System.out.println("Sum " + a1 + " and " + b1 + " = " + expected1);
        try {
            System.out.println("Result = " + calculator.sum(a1, b1));
            assertEquals(expected1, calculator.sum(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/sum.csv", numLinesToSkip = 1)
    public void sumCharA_with_ChartB(String expected, String a, String b) {
        char a1 = (char) Integer.parseInt(a);
        char b1 = (char) Integer.parseInt(b);
        int expected1 = Integer.parseInt(expected);
        System.out.println("Sum " + a1 + " and " + b1 + " = " + expected1);
        try {
            System.out.println("Result = " + calculator.sum(a1, b1));
            assertEquals(expected1, calculator.sum(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/sum.csv", numLinesToSkip = 1)
    public void sumByteA_with_ByteB(String expected, String a, String b) {
        byte a1 = Byte.parseByte(a);
        byte b1 = Byte.parseByte(b);
        int expected1 = Integer.parseInt(expected);
        System.out.println("Sum " + a1 + " and " + b1 + " = " + expected1);
        try {
            System.out.println("Result = " + calculator.sum(a1, b1));
            assertEquals(expected1, calculator.sum(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/sum.csv", numLinesToSkip = 1)
    public void sumLongA_with_LongB(String expected, String a, String b) {
        long a1 = Long.parseLong(a);
        long b1 = Long.parseLong(b);
        long expected1 = Long.parseLong(expected);
        System.out.println("Sum " + a1 + " and " + b1 + " = " + expected1);
        try {
            System.out.println("Result = " + calculator.sum(a1, b1));
            assertEquals(expected1, calculator.sum(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/sum.csv", numLinesToSkip = 1)
    public void sumStringA_with_StringB(String expected, String a, String b) {
        float expected1 = Float.parseFloat(expected);
        System.out.println("Sum " + a + " and " + b + " = " + expected);
        try {
            System.out.println("Result = " + calculator.sum(a, b));
            assertEquals(expected1, calculator.sum(a, b), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/sum.csv", numLinesToSkip = 1)
    public void sumIntA_with_DoubleB(String expected, String a, String b) {
        int a1 = Integer.parseInt(a);
        double b1 = Double.parseDouble(b);
        double expected1 = Double.parseDouble(expected);
        System.out.println("Sum " + a1 + " and " + b1 + " = " + expected1);
        try {
            System.out.println("Result = " + calculator.sum(a1, b1));
            assertEquals(expected1, calculator.sum(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/sum.csv", numLinesToSkip = 1)
    public void sumIntA_with_FloatB(String expected, String a, String b) {
        int a1 = Integer.parseInt(a);
        float b1 = Float.parseFloat(b);
        float expected1 = Float.parseFloat(expected);
        System.out.println("Sum " + a1 + " and " + b1 + " = " + expected1);
        try {
            System.out.println("Result = " + calculator.sum(a1, b1));
            assertEquals(expected1, calculator.sum(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/sum.csv", numLinesToSkip = 1)
    public void sumIntA_with_StringB(String expected, String a, String b) {
        int a1 = Integer.parseInt(a);
        double expected1 = Double.parseDouble(expected);
        System.out.println("Sum " + a1 + " and " + b + " = " + expected1);
        try {
            System.out.println("Result = " + calculator.sum(a1, b));
            assertEquals(expected1, calculator.sum(a1, b), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/sum.csv", numLinesToSkip = 1)
    public void sumIntA_with_ShortB(String expected, String a, String b) {
        int a1 = Integer.parseInt(a);
        short b1 = Short.parseShort(b);
        double expected1 = Double.parseDouble(expected);
        System.out.println("Sum " + a1 + " and " + b1 + " = " + expected1);
        try {
            System.out.println("Result = " + calculator.sum(a1, b));
            assertEquals(expected1, calculator.sum(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/sum.csv", numLinesToSkip = 1)
    public void sumIntA_with_CharB(String expected, String a, String b) {
        int a1 = Integer.parseInt(a);
        char b1 = (char) Integer.parseInt(b);
        double expected1 = Double.parseDouble(expected);
        System.out.println("Sum " + a1 + " and " + b1 + " = " + expected1);
        try {
            System.out.println("Result = " + calculator.sum(a1, b));
            assertEquals(expected1, calculator.sum(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/sum.csv", numLinesToSkip = 1)
    public void sumIntA_with_ByteB(String expected, String a, String b) {
        int a1 = Integer.parseInt(a);
        byte b1 = Byte.parseByte(b);
        double expected1 = Double.parseDouble(expected);
        System.out.println("Sum " + a1 + " and " + b1 + " = " + expected1);
        try {
            System.out.println("Result = " + calculator.sum(a1, b));
            assertEquals(expected1, calculator.sum(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/sum.csv", numLinesToSkip = 1)
    public void sumIntA_with_LongB(String expected, String a, String b) {
        int a1 = Integer.parseInt(a);
        long b1 = Long.parseLong(b);
        double expected1 = Double.parseDouble(expected);
        System.out.println("Sum " + a1 + " and " + b1 + " = " + expected1);
        try {
            System.out.println("Result = " + calculator.sum(a1, b));
            assertEquals(expected1, calculator.sum(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/sum.csv", numLinesToSkip = 1)
    public void sumDoubleA_with_IntB(String expected, String a, String b) {
        double a1 = Double.parseDouble(a);
        int b1 = Integer.parseInt(b);
        double expected1 = Double.parseDouble(expected);
        System.out.println("Sum " + a1 + " and " + b1 + " = " + expected1);
        try {
            System.out.println("Result = " + calculator.sum(a1, b));
            assertEquals(expected1, calculator.sum(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/sum.csv", numLinesToSkip = 1)
    public void sumFloatA_with_IntB(String expected, String a, String b) {
        float a1 = Float.parseFloat(a);
        int b1 = Integer.parseInt(b);
        double expected1 = Double.parseDouble(expected);
        System.out.println("Sum " + a1 + " and " + b1 + " = " + expected1);
        try {
            System.out.println("Result = " + calculator.sum(a1, b));
            assertEquals(expected1, calculator.sum(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/sum.csv", numLinesToSkip = 1)
    public void sumStringA_with_IntB(String expected, String a, String b) {
        int b1 = Integer.parseInt(b);
        double expected1 = Double.parseDouble(expected);
        System.out.println("Sum " + a + " and " + b1 + " = " + expected1);
        try {
            System.out.println("Result = " + calculator.sum(a, b1));
            assertEquals(expected1, calculator.sum(a, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/sum.csv", numLinesToSkip = 1)
    public void sumShortA_with_IntB(String expected, String a, String b) {
        short a1 = Short.parseShort(a);
        int b1 = Integer.parseInt(b);
        double expected1 = Double.parseDouble(expected);
        System.out.println("Sum " + a1 + " and " + b1 + " = " + expected1);
        try {
            System.out.println("Result = " + calculator.sum(a1, b1));
            assertEquals(expected1, calculator.sum(a, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/sum.csv", numLinesToSkip = 1)
    public void sumCharA_with_IntB(String expected, String a, String b) {
        char a1 = (char) Integer.parseInt(a);
        int b1 = Integer.parseInt(b);
        double expected1 = Double.parseDouble(expected);
        System.out.println("Sum " + a1 + " and " + b1 + " = " + expected1);
        try {
            System.out.println("Result = " + calculator.sum(a1, b1));
            assertEquals(expected1, calculator.sum(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/sum.csv", numLinesToSkip = 1)
    public void sumByteA_with_IntB(String expected, String a, String b) {
        byte a1 = Byte.parseByte(a);
        int b1 = Integer.parseInt(b);
        double expected1 = Double.parseDouble(expected);
        System.out.println("Sum " + a1 + " and " + b1 + " = " + expected1);
        try {
            System.out.println("Result = " + calculator.sum(a1, b1));
            assertEquals(expected1, calculator.sum(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/sum.csv", numLinesToSkip = 1)
    public void sumLongA_with_IntB(String expected, String a, String b) {
        long a1 = Long.parseLong(a);
        int b1 = Integer.parseInt(b);
        double expected1 = Double.parseDouble(expected);
        System.out.println("Sum " + a1 + " and " + b1 + " = " + expected1);
        try {
            System.out.println("Result = " + calculator.sum(a1, b1));
            assertEquals(expected1, calculator.sum(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }
    @ParameterizedTest
    @CsvFileSource(resources = "/sum.csv", numLinesToSkip = 1)
    public void sumDoubleA_with_FloatB(String expected, String a, String b) {
        double a1 = Double.parseDouble(a);
        float b1 = Float.parseFloat(b);
        double expected1 = Double.parseDouble(expected);
        System.out.println("Sum " + a1 + " and " + b1 + " = " + expected1);
        try {
            System.out.println("Result = " + calculator.sum(a1, b1));
            assertEquals(expected1, calculator.sum(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/sum.csv", numLinesToSkip = 1)
    public void sumDoubleA_with_StringB(String expected, String a, String b) {
        double a1 = Double.parseDouble(a);
        double expected1 = Double.parseDouble(expected);
        System.out.println("Sum " + a1 + " and " + b + " = " + expected1);
        try {
            System.out.println("Result = " + calculator.sum(a1, b));
            assertEquals(expected1, calculator.sum(a1, b), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/sum.csv", numLinesToSkip = 1)
    public void sumDoubleA_with_ShortB(String expected, String a, String b) {
        double a1 = Double.parseDouble(a);
        short b1 = Short.parseShort(b);
        double expected1 = Double.parseDouble(expected);
        System.out.println("Sum " + a1 + " and " + b1 + " = " + expected1);
        try {
            System.out.println("Result = " + calculator.sum(a1, b1));
            assertEquals(expected1, calculator.sum(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/sum.csv", numLinesToSkip = 1)
    public void sumDoubleA_with_CharB(String expected, String a, String b) {
        double a1 = Double.parseDouble(a);
        char b1 = (char) Integer.parseInt(b);
        double expected1 = Double.parseDouble(expected);
        System.out.println("Sum " + a1 + " and " + b1 + " = " + expected1);
        try {
            System.out.println("Result = " + calculator.sum(a1, b1));
            assertEquals(expected1, calculator.sum(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/sum.csv", numLinesToSkip = 1)
    public void sumDoubleA_with_ByteB(String expected, String a, String b) {
        double a1 = Double.parseDouble(a);
        byte b1 = Byte.parseByte(b);
        double expected1 = Double.parseDouble(expected);
        System.out.println("Sum " + a1 + " and " + b1 + " = " + expected1);
        try {
            System.out.println("Result = " + calculator.sum(a1, b1));
            assertEquals(expected1, calculator.sum(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/sum.csv", numLinesToSkip = 1)
    public void sumDoubleA_with_LongB(String expected, String a, String b) {
        double a1 = Double.parseDouble(a);
        long b1 = Long.parseLong(b);
        double expected1 = Double.parseDouble(expected);
        System.out.println("Sum " + a1 + " and " + b1 + " = " + expected1);
        try {
            System.out.println("Result = " + calculator.sum(a1, b1));
            assertEquals(expected1, calculator.sum(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }


    @ParameterizedTest
    @CsvFileSource(resources = "/sum.csv", numLinesToSkip = 1)
    public void sumFloatA_with_DoubleB(String expected, String a, String b) {
        float a1 = Float.parseFloat(a);
        double b1 = Double.parseDouble(b);
        double expected1 = Double.parseDouble(expected);
        System.out.println("Sum " + a1 + " and " + b1 + " = " + expected1);
        try {
            System.out.println("Result = " + calculator.sum(a1, b1));
            assertEquals(expected1, calculator.sum(a1, b1), 0.01);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }



}




import dzmitry.klokau.testing.Calculator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


public class CalcTests {

    private Calculator calculator;

    @Before
    public void initTest() {
        calculator = new Calculator();
    }

    @After
    public void afterTest(){
        calculator = null;
    }

    @Test
    public void sum_IntA_And_IntB_positive() {
        assertEquals(10, calculator.sum(5,5), 0.01);
    }

    @Test
    public void testSum_IntA_And_IntB_negative() {
        assertNotEquals(12,calculator.sum(5,5), 0.01);
    }

    @Test
    public void testSum_IntA_And_IntB_BoundaryValue() {
        assertEquals(-1, calculator.sum(-2147483648, 2147483647), 0.000000001);
        assertNotEquals(-2, calculator.sum(-2147483648, 2147483647), 0.000000001);
    }

    @Test
    public void testSumOfZeros() {
        assertEquals(0, calculator.sum(0,0), 0.000000001);
    }

    @Test(expected = NumberFormatException.class)
    public void testSumIntWithIncorrectValue() {
        assertEquals(10, calculator.sum("qwerty",5));
    }

    @Test
    public void sumCharA_And_IntB_positive() {
        assertEquals(194, calculator.sum(97, 97));
    }

    @Test
    public void sumCharA_And_IntB_negative() {
        assertNotEquals(194, calculator.sum(95, 97));
    }

    @Test(expected = NumberFormatException.class)
    public void sumCharIncorrectValue() {
        assertEquals(194, calculator.sum("a", 97));
    }

    @Test
    public void sumByteA_And_IntB_positive() {
        assertEquals(2147483519,calculator.sum(-128, 2147483647));
    }

    @Test
    public void sumByteA_And_IntB_negative() {
        assertEquals(2144783775,calculator.sum(130, 2147483647), "Error!");
    }

    @Test
    public void sumIntA_And_CharB_positive() {
        assertEquals(2147483519, calculator.sum(2147483647, -128)) ;
    }

    @Test
    public void sumIntA_And_CharB_negative() {
        assertNotEquals(2147483519, calculator.sum(2147483647, 130));
    }

    @Test
    public void divisionByteA_And_IntB_positive() {
        assertEquals(-21.3333333333, calculator.division(-128, 6), "Error!");
    }

    @Test(expected = ArithmeticException.class)
    public void divisionByZeroWillThrowException() {
        assertEquals(0, calculator.division(10,0),"You cannot divide by zero!");
    }

    @Test
    public void divisionZeroByNumber() {
        assertEquals(0, calculator.division(0, 10));
    }

    // @Ignore("Ignored test")
    @Test
    public void divisionByteA_And_CharB() {
        assertEquals(-1, calculator.division(-128, 128), 0.01);
    }

    // @Ignore("Ignored test")
    @Test
    public void divisionIntByDouble() {
        assertEquals(100, calculator.division(10, 0.1));
    }

    @Test
    public void subtractionIntA_And_intB() {
        assertEquals(-1, calculator.subtraction(-2147483648, 2147483647));
    }

    @Test
    public void multiplicationByteA_And_IntB() {
        assertEquals(127, calculator.multiplication(127, 1));
    }

    @Test
    public void multiplicationByZero() {
        assertEquals(0, calculator.multiplication(2147483647, 0));
    }

}









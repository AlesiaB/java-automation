## **Задание**
[](https://bitbucket.org/AlesiaB/java-automation/src/unit_testing_framework/homeworks/unit_testing_framework/task3.jpg)

### Результаты работы
![](https://bitbucket.org/AlesiaB/java-automation/src/unit_testing_framework/homeworks/unit_testing_framework/task3_pictures/divTests.png "divTests")​
![](https://bitbucket.org/AlesiaB/java-automation/src/unit_testing_framework/homeworks/unit_testing_framework/task3_pictures/multiTests.png "multuTests")​
![](https://bitbucket.org/AlesiaB/java-automation/src/unit_testing_framework/homeworks/unit_testing_framework/task3_pictures/subTests.png "subTests")​
![](https://bitbucket.org/AlesiaB/java-automation/src/unit_testing_framework/homeworks/unit_testing_framework/task3_pictures/sumTests.png "sumTests")​


### **Основные классы**
- [CalculatorDivisionTests.java](https://bitbucket.org/AlesiaB/java-automation/src/unit_testing_framework/homeworks/unit_testing_framework/calculator_junit/src/test/java/CalculatorDivisionTests.java)
- [CalculatorMultTests.java](https://bitbucket.org/AlesiaB/java-automation/src/unit_testing_framework/homeworks/unit_testing_framework/calculator_junit/src/test/java/CalculatorMultTests.java)
- [CalculatorSubTests.java](https://bitbucket.org/AlesiaB/java-automation/src/unit_testing_framework/homeworks/unit_testing_framework/calculator_junit/src/test/java/CalculatorSubTests.java)
- [CalculatorSumTests.java](https://bitbucket.org/AlesiaB/java-automation/src/unit_testing_framework/homeworks/unit_testing_framework/calculator_junit/src/test/java/CalculatorSumTests.java)
- [CalcTests.java](https://bitbucket.org/AlesiaB/java-automation/src/unit_testing_framework/homeworks/unit_testing_framework/calculator_junit/src/test/java/CalcTests.java)